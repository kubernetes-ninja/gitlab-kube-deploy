FROM docker:dind

# Install requirements
RUN apk add -U openssl curl tar gzip python bash ca-certificates && \
  wget -q -O /etc/apk/keys/sgerrand.rsa.pub https://alpine-pkgs.sgerrand.com/sgerrand.rsa.pub && \
  wget https://github.com/sgerrand/alpine-pkg-glibc/releases/download/2.29-r0/glibc-2.29-r0.apk && \
  apk add glibc-2.29-r0.apk && \
  rm glibc-2.29-r0.apk


# Ruby is required for reading CI_ENVIRONMENT_URL from .gitlab-ci.yml
RUN apk add ruby git py3-pip

# Install Helm
#RUN curl https://kubernetes-helm.storage.googleapis.com/helm-v2.0.2-linux-amd64.tar.gz | \
#  tar zx && mv linux-amd64/helm /usr/bin/ && \
#  helm version --client

## Install Helm Canary
#RUN curl https://kubernetes-helm.storage.googleapis.com/helm-canary-linux-amd64.tar.gz | \
#  tar zx && mv linux-amd64/helm /usr/bin/ && \
#  helm version --client

# Install kubectl
RUN curl -L -o /usr/bin/kubectl https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/latest.txt)/bin/linux/amd64/kubectl && \
  chmod +x /usr/bin/kubectl && \
  kubectl version --client

# Install deploy scripts
ENV PATH=/opt/kubernetes-deploy:$PATH
COPY / /opt/kubernetes-deploy/

# Install awscli
RUN  set -ex \
        && pip3 install --upgrade pip \
        && pip install awscli --upgrade --user

# Install aws-iam-authenticator
RUN set -ex \
          && curl -o aws-iam-authenticator https://amazon-eks.s3-us-west-2.amazonaws.com/1.14.6/2019-08-22/bin/linux/amd64/aws-iam-authenticator \
          && mv ./aws-iam-authenticator /root/.local/bin/ \
          && chmod +x /root/.local/bin/aws-iam-authenticator

ENV PATH=$PATH:/root/.local/bin

ADD confs/kubeconfig /tmp/kubeconfig
RUN set -ex \
      && mkdir -p ${HOME}/.kube \
      && cp /tmp/kubeconfig ${HOME}/.kube/config


RUN ln -s /opt/kubernetes-deploy/run /usr/bin/deploy && \
  which deploy && \
  which canary && \
  which build && \
  which destroy

ENTRYPOINT []
CMD []
